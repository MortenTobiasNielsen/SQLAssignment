USE [SuperheroDb]
GO

SET ANSI_NULLS ON
GO

CREATE TABLE SuperheroPower (
	SuperheroId int FOREIGN KEY REFERENCES Superhero(Id),
	PowerId int FOREIGN KEY REFERENCES Power(Id),
	PRIMARY KEY (SuperheroId, PowerId)
);
GO