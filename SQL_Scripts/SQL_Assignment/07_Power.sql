USE [SuperheroDb]
GO

INSERT INTO Power(Name, Description)
VALUES(N'Flying', N'The ability to fly')

INSERT INTO Power(Name, Description)
VALUES(N'Genius level intellect', N'Very intelligent')

INSERT INTO Power(Name, Description)
VALUES(N'Powered armor suit', N'Provides many powers')

INSERT INTO Power(Name, Description)
VALUES(N'Master hand-to-hand combatant', N'Good in close combat')
GO

-- Flying to Ironman and Thor
INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(1, 1)

INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(1, 3)

-- Genius level intellect to Ironman and Batman
INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(2, 1)

INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(2, 2)

-- Powered armor suit to Ironman
INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(3, 1)

-- Master hand-to-hand combatant to Batman and Thor
INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(4, 2)

INSERT INTO SuperheroPower(PowerId, SuperheroId)
VALUES(4, 3)
GO