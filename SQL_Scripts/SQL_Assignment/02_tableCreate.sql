USE [SuperheroDb]
GO

/*******************************************************************************
   Create Tables
********************************************************************************/
SET ANSI_NULLS ON
GO

CREATE TABLE Superhero (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(25) NOT NULL,
	Alias nvarchar(25) NOT NULL,
	Origin nvarchar(100) NOT NULL,
);
GO

SET ANSI_NULLS ON
GO

CREATE TABLE Assistant (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(25) NOT NULL,
);
GO

SET ANSI_NULLS ON
GO

CREATE TABLE Power (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NOT NULL,
	Description nvarchar(100) NOT NULL,
);
GO