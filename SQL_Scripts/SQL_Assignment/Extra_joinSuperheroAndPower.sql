USE [SuperheroDb]
GO

SELECT 
	Power.Name AS PowerName, 
	Power.Description As PowerDescription,
	Superhero.Name AS SuperheroName,
	Superhero.Origin AS SuperheroOrigin
FROM Power
JOIN SuperheroPower
	ON Power.Id = SuperheroPower.PowerId
JOIN Superhero
	ON SuperheroPower.SuperheroId = Superhero.Id
ORDER BY PowerName
GO