USE [SuperheroDb]
GO

SET ANSI_NULLS ON
GO

ALTER TABLE Assistant
ADD SuperheroId int FOREIGN KEY REFERENCES Superhero(Id)
GO