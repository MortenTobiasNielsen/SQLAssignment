USE [master]
GO

/*******************************************************************************
   Drop database if it exists
********************************************************************************/
IF EXISTS (SELECT N'SuperheroDb' FROM master.dbo.sysdatabases)
BEGIN
	ALTER DATABASE [SuperheroDb] SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE [SuperheroDb] SET ONLINE;
	DROP DATABASE [SuperheroDb];
END

GO

/*******************************************************************************
   Create database
********************************************************************************/
CREATE DATABASE [SuperheroDb];
GO