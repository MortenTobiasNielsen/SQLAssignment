USE [SuperheroDb]
GO

INSERT INTO Superhero (Name, Alias, Origin)
VALUES(N'Ironman', N'Tony Stark', N'Genius, Billionaire, Playboy, Philanthropist')

INSERT INTO Superhero (Name, Alias, Origin)
VALUES(N'Batman', N'Bruce Wayne', N'Wealthy American industrialist')

INSERT INTO Superhero (Name, Alias, Origin)
VALUES(N'THOR', N'God of Thunder', N'King of New Asgard')