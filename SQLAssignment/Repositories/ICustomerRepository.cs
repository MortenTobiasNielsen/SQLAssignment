﻿using SQLAssignment.Models;
using System;
using System.Collections.Generic;

namespace SQLAssignment.Repositories {
    public interface ICustomerRepository {
        /// <summary>
        /// Get a specific customer based on the customer id from the customer table
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A customer object with the customers data</returns>
        public Customer GetCustomer(string id);

        /// <summary>
        /// Get the first customer in the customer table which first name is like the first name provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A customer object with the customers data</returns>
        /// 
        public Customer GetCustomerByFirstName(string firstName);

        /// <summary>
        /// Gets all the customers in the customer table
        /// </summary>
        /// <returns>A list of the customers from the database</returns>
        public List<Customer> GetAllCustomers();

        /// <summary>
        /// The customer table will first be ordered by first name and then a specified amount of customers retreived, with an offset. 
        /// </summary>
        /// <returns>A list of the customers from the database</returns>
        public List<Customer> GetCustomersLimited(int limit, int offset);

        /// <summary>
        /// Adds the specified customer to the customer table
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>A boolean indicating success or failure</returns>
        public bool AddNewCustomer(Customer customer);

        /// <summary>
        /// Updates the specified customer in the customer table
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>A boolean indicating success or failure</returns>
        public bool UpdateCustomer(Customer customer);

        /// <summary>
        /// Provides the amount of customers per country in decending order
        /// </summary>
        /// <returns>A CustomerCountry class which has a CustomersPerCountry dictionary field</returns>
        public CustomerCountry GetCustomersByCountryDescinding();

        /// <summary>
        /// Provides the highest spender (ID, first name and last name) and the amount spend in decending order
        /// </summary>
        /// <returns>A CustomerSpender class which has a HighestSpender dictionary field</returns>
        public CustomerSpender GetHighestCustomerSpender();

        /// <summary>
        /// Provides the most popular genre for a specifc customer. Multiple genres can be returned if there are multiple genres, which has been purchased equally much.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A CustomerGenre class which has a list of the MostPopularGenres</returns>
        public CustomerGenre GetMostPopularGenre(string id);
    }
}
