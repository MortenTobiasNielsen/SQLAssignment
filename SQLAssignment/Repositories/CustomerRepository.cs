﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using SQLAssignment.Models;
using SQLAssignment.Utility;

namespace SQLAssignment.Repositories {
    public class CustomerRepository : ICustomerRepository {
        private static Customer ExtractCustomer (SqlDataReader reader) {
            return new() { 
                Id = reader.GetInt32(0),
                FirstName = Utilities.SafeGetString(reader, 1), 
                LastName = Utilities.SafeGetString(reader, 2),
                Country = Utilities.SafeGetString(reader, 3),
                PostalCode = Utilities.SafeGetString(reader, 4),
                PhoneNumber = Utilities.SafeGetString(reader, 5),
                Email = Utilities.SafeGetString(reader, 6)
            };
        } 

        public bool AddNewCustomer(Customer customer) {
            string sql = @"INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)
                           VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            
            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                command.Parameters.AddWithValue("@Email", customer.Email);

                Utilities.NullConversion(command);

                command.ExecuteNonQuery();

                return true;
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
                return false;
            }
        }

        public List<Customer> GetAllCustomers() {
            List<Customer> allCustomersList = new();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                           FROM Customer";

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read()) {
                    Customer customer = ExtractCustomer(reader);

                    allCustomersList.Add(customer);
                }

            } catch (SqlException ex) {

                Console.WriteLine("Error: " + ex);
            }

            return allCustomersList;
        }

        public Customer GetCustomer(string id) {
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                           FROM Customer 
                           WHERE CustomerId = @CustomerID";

            Customer customer = new();

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                command.Parameters.AddWithValue("@CustomerID", id);

                using SqlDataReader reader = command.ExecuteReader();

                if (reader.Read()) {
                    customer = ExtractCustomer(reader);
                };
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
            }

            return customer;
        }
        public Customer GetCustomerByFirstName(string firstName) {
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                           FROM Customer 
                           WHERE FirstName LIKE @firstName + '%'";

            Customer customer = new();

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                command.Parameters.AddWithValue("@firstName", firstName);

                using SqlDataReader reader = command.ExecuteReader();

                if (reader.Read()) {
                    customer = ExtractCustomer(reader);
                };
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
            }

            return customer;
        }

        public CustomerCountry GetCustomersByCountryDescinding() {
            string sql = @"SELECT COUNT (CustomerId), Country 
                           FROM Customer 
                           GROUP BY Country 
                           ORDER BY COUNT(CustomerId) DESC";

            CustomerCountry customerCountry = new();

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read()) {
                    int customers = reader.GetInt32(0);
                    string country = Utilities.SafeGetString(reader, 1);

                    if (country == null) country = "Unknown";

                    customerCountry.CustomersPerCountry.Add(country, customers);
                };
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
            }

            return customerCountry;
        }

        public List<Customer> GetCustomersLimited(int limit, int offset) {
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                           FROM Customer 
                           ORDER BY FirstName 
                           OFFSET (@offset) ROWS 
                           FETCH NEXT (@limit) ROWS ONLY";

            List<Customer> allCustomersList = new();

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                command.Parameters.AddWithValue("@limit", limit);
                command.Parameters.AddWithValue("@offset", offset);

                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read()) {
                    Customer customer = ExtractCustomer(reader);

                    allCustomersList.Add(customer);
                };
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
            }

            return allCustomersList;
        }

        public CustomerSpender GetHighestCustomerSpender() {
            string sql = @"SELECT Invoice.CustomerId, Customer.FirstName, Customer.LastName, SUM (Total) AS AmountSpend
                        FROM Invoice 
                        JOIN Customer 
	                        ON Invoice.CustomerId = Customer.CustomerId 
                        GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName 
                        ORDER BY SUM(Total) DESC";

            CustomerSpender customerSpender = new();

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                using SqlDataReader reader = command.ExecuteReader();

                while (reader.Read()) {
                    string Identifier = $"{reader.GetInt32(0)} {Utilities.SafeGetString(reader, 1)} {Utilities.SafeGetString(reader, 2)}";
                    decimal amountSpend = reader.GetDecimal(3);

                    customerSpender.HighestSpenders.Add(Identifier, amountSpend);
                };
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
            }

            return customerSpender;
        }

        public CustomerGenre GetMostPopularGenre(string id) {
            string sql = @"SELECT Customer.CustomerId, Genre.Name, COUNT(Genre.Name) As #
                        FROM InvoiceLine
                        JOIN Track
	                        ON Track.TrackId = InvoiceLine.TrackId
                        JOIN Genre
	                        ON Track.GenreId = Genre.GenreId
                        JOIN Invoice
	                        ON Invoice.InvoiceId = InvoiceLine.InvoiceId
                        JOIN Customer
	                        ON Customer.CustomerId = Invoice.CustomerId
                        WHERE
	                        Customer.CustomerId = @CustomerId
                        GROUP BY
	                        Customer.CustomerId, Genre.Name
                        ORDER BY
	                        Customer.CustomerId, # Desc";

            CustomerGenre customerGenre = new();

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                command.Parameters.AddWithValue("@CustomerID", id);

                using SqlDataReader reader = command.ExecuteReader();

                int genreAmount = 0;
                while (reader.Read()) {
                    if (genreAmount > reader.GetInt32(2)) break;

                    customerGenre.MostPopularGenre.Add(Utilities.SafeGetString(reader, 1));
                    genreAmount = reader.GetInt32(2);

                };
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
            }

            return customerGenre;
        }

        public bool UpdateCustomer(Customer customer) {
            string sql = @"UPDATE Customer 
                           SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email 
                           WHERE CustomerId = @CustomerID";

            try {
                using SqlConnection connection = new(ConnectionHelper.GetConnectionstring());
                connection.Open();

                using SqlCommand command = new(sql, connection);
                command.Parameters.AddWithValue("@CustomerID", customer.Id);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                command.Parameters.AddWithValue("@Email", customer.Email);

                Utilities.NullConversion(command);

                command.ExecuteNonQuery();

                return true;
            } catch (SqlException ex) {
                Console.WriteLine("Error: " + ex);
                return false;
            }
        }
    }
}
