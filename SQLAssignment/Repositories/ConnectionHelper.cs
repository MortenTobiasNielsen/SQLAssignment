﻿using Microsoft.Data.SqlClient;

namespace SQLAssignment.Repositories {
    public static class ConnectionHelper {
        /// <summary>
        /// Constructs the connection string to be used for connecting to the SQL server
        /// </summary>
        /// <returns>A string which can be used to connect to a specifc SQL server </returns>
        public static string GetConnectionstring() {
            SqlConnectionStringBuilder connectionStringBuilder = new ();
            connectionStringBuilder.DataSource = @"PC1693\SQLEXPRESS";
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            return connectionStringBuilder.ConnectionString;
        }
    }
}
