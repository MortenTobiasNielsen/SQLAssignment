﻿using SQLAssignment.Models;
using SQLAssignment.Repositories;
using SQLAssignment.Utility;
using System;
using System.Collections.Generic;

namespace SQLAssignment {
    class Program {
        static void Main() {
            ICustomerRepository repository = new CustomerRepository();

            // This is just here to test that the functionality works as intended

            Seperator();
            Console.WriteLine("TestGetAllCustomers:\n");
            List<Customer> customers1 = TestGetAllCustomers(repository);
            foreach (Customer customer in customers1) {
                Utilities.PrintCustomerName(customer);
            }

            Seperator();
            Console.WriteLine("TestGetCustomer:\n");
            Utilities.PrintCustomerName(TestGetCustomer(repository, "4"));

            Seperator();
            Console.WriteLine("TestGetCustomerByFirstName:\n");
            Utilities.PrintCustomerName(TestGetCustomerByFirstName(repository, "Fran"));

            Seperator();
            Console.WriteLine("TestGetCustomersLimited:\n");
            List<Customer> customers2 = TestGetCustomersLimited(repository, 5, 2);
            foreach (Customer customer in customers2) {
                Utilities.PrintCustomerName(customer);
            }

            Seperator();
            Console.WriteLine("TestAddNewCustomer:\n");
            Customer Newcustomer = new() { FirstName = "Test", LastName = "Test", Email = "Test@Test.com" };
            if (TestAddNewCustomer(repository, Newcustomer)) {
                Console.WriteLine("Success - Added new customer");
            } else {
                Console.WriteLine("Failure - Didn't Add new customer");
            }

            Seperator();
            Console.WriteLine("TestUpdateCustomer:\n");
            Customer Updatecustomer = new() { Id = 5, FirstName = "Test", LastName = "Test", Email = "Test@Test.com" };
            if (TestUpdateCustomer(repository, Updatecustomer)) {
                Console.WriteLine("Success - Updated customer");
            } else {
                Console.WriteLine("Failure - Didn't update customer");
            }

            Seperator();
            Console.WriteLine("TestGetCustomersByCountryDescinding:\n");
            CustomerCountry customerCountry = TestGetCustomersByCountryDescinding(repository);
            foreach (var countryCount in customerCountry.CustomersPerCountry) {
                Console.WriteLine($"{countryCount.Key}: {countryCount.Value}");
            }

            Seperator();
            Console.WriteLine("TestGetHighestCustomerSpender:\n");
            CustomerSpender customerSpender = TestGetHighestCustomerSpender(repository);
            foreach (var amountsSpend in customerSpender.HighestSpenders) {
                Console.WriteLine($"{amountsSpend.Key} Amount spend: {amountsSpend.Value}");
            }

            Seperator();
            Console.WriteLine("TestGetMostPopularGenre:\n");
            string customerId = "12";
            CustomerGenre customerGenre = TestGetMostPopularGenre(repository, customerId);
            Console.WriteLine($"The most popular genre for Customer {customerId}:");
            foreach (string genre in customerGenre.MostPopularGenre) {
                Console.WriteLine(genre);
            }
        }

        public static List<Customer> TestGetAllCustomers(ICustomerRepository repository) {
            return repository.GetAllCustomers();
        }
        
        public static Customer TestGetCustomer(ICustomerRepository repository, string id) {
            return repository.GetCustomer(id);
        }

        public static Customer TestGetCustomerByFirstName(ICustomerRepository repository, string name) {
           return repository.GetCustomerByFirstName(name);
        }

        public static List<Customer> TestGetCustomersLimited(ICustomerRepository repository, int limit, int offset) {
            return repository.GetCustomersLimited(limit, offset);
        }

        public static bool TestAddNewCustomer(ICustomerRepository repository, Customer customer) {
            return repository.AddNewCustomer(customer);
        }

        public static bool TestUpdateCustomer(ICustomerRepository repository, Customer customer) {
            return repository.UpdateCustomer(customer);
        }

        public static CustomerCountry TestGetCustomersByCountryDescinding(ICustomerRepository repository) {
            return repository.GetCustomersByCountryDescinding();
        }

        public static CustomerSpender TestGetHighestCustomerSpender(ICustomerRepository repository) {
            return repository.GetHighestCustomerSpender();
        }
        public static CustomerGenre TestGetMostPopularGenre(ICustomerRepository repository, string id) {
            return repository.GetMostPopularGenre(id);
        }

        private static void Seperator () {
            
            Console.WriteLine("\n|-----------------------------------------------------------------------------------------------|\n");
        }
    }
}
