﻿using Microsoft.Data.SqlClient;
using SQLAssignment.Models;
using System;
using System.Data;

namespace SQLAssignment.Utility {
    public static class Utilities {
        /// <summary>
        /// Checks if the input from the database is null
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="index"></param>
        /// <returns>The string from the database or null if the database value is null</returns>
        public static string SafeGetString(SqlDataReader reader, int index) {
            if (reader.IsDBNull(index)) {
                return null;
            } 

            return reader.GetString(index);
        }

        /// <summary>
        /// Prints the first name of the customer
        /// </summary>
        /// <param name="customer"></param>
        public static void PrintCustomerName(Customer customer) {
            Console.WriteLine(customer.FirstName);
        }

        /// <summary>
        /// Performs an inplace conversion of null values, so they are understandable for the database.
        /// </summary>
        /// <param name="command"></param>
        public static void NullConversion (SqlCommand command) {
            foreach (IDataParameter param in command.Parameters) {
                if (param.Value == null) param.Value = DBNull.Value;
            }
        }
    }
}
