﻿using System.Collections.Generic;

namespace SQLAssignment.Models {
    /// <summary>
    /// Represents country related data per Customer in the database
    /// </summary>
    public class CustomerCountry {
        public Dictionary<string, int> CustomersPerCountry { get; set; } = new Dictionary<string, int>();
    }
}
