﻿using System.Collections.Generic;

namespace SQLAssignment.Models {
    /// <summary>
    /// Represents genre related data per Customer in the database
    /// </summary>
    public class CustomerGenre {
        public List<string> MostPopularGenre { get; set; } = new();
    }
}
