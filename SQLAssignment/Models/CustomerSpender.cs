﻿using System.Collections.Generic;

namespace SQLAssignment.Models {
    /// <summary>
    /// Represents spending related data per Customer in the database
    /// </summary>
    public class CustomerSpender {
        public Dictionary<string, decimal> HighestSpenders { get; set; } = new();
    }
}
